﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Football_Academy_API.DTOs.Coach
{
    public class GetCoachDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<string> Players { get; set; }
    }
}
