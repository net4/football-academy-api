﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Football_Academy_API.DTOs.CoachPlayer
{
    public class GetCoachPlayersDTO
    {
        public int PlayerId { get; set; }
        public int CoachId { get; set; }
    }
}
