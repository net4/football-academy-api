﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Football_Academy_API.Migrations
{
    public partial class CoachPlayers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoachAthletes");

            migrationBuilder.CreateTable(
                name: "CoachPlayers",
                columns: table => new
                {
                    PlayerId = table.Column<int>(nullable: false),
                    CoachId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoachPlayers", x => new { x.PlayerId, x.CoachId });
                    table.ForeignKey(
                        name: "FK_CoachPlayers_Coaches_CoachId",
                        column: x => x.CoachId,
                        principalTable: "Coaches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoachPlayers_Players_PlayerId",
                        column: x => x.PlayerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CoachPlayers_CoachId",
                table: "CoachPlayers",
                column: "CoachId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoachPlayers");

            migrationBuilder.CreateTable(
                name: "CoachAthletes",
                columns: table => new
                {
                    PlayerId = table.Column<int>(type: "int", nullable: false),
                    CoachId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoachAthletes", x => new { x.PlayerId, x.CoachId });
                    table.ForeignKey(
                        name: "FK_CoachAthletes_Coaches_CoachId",
                        column: x => x.CoachId,
                        principalTable: "Coaches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoachAthletes_Players_PlayerId",
                        column: x => x.PlayerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CoachAthletes_CoachId",
                table: "CoachAthletes",
                column: "CoachId");
        }
    }
}
