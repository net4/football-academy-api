﻿using AutoMapper;
using Football_Academy_API.DTOs.Coach;
using Football_Academy_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Football_Academy_API.Profiles
{
    public class CoachProfile : Profile
    {
        public CoachProfile()
        {
            CreateMap<Coach, GetCoachDTO>()
                .ForMember(dst => dst.Players, src => src.MapFrom(p => p.CoachPlayers.Select(cp => cp.Player.FirstName + " " + cp.Player.LastName)))
                .ForMember(dst => dst.Name, src => src.MapFrom(cp => cp.FirstName + " " + cp.LastName));
        }
    }
}
