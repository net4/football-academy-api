﻿using AutoMapper;
using Football_Academy_API.DTOs.Player;
using Football_Academy_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Football_Academy_API.Profiles
{
    public class PlayerProfile : Profile
    {
        public PlayerProfile()
        {
            CreateMap<Player, GetPlayerDTO>()
                .ForMember(dst => dst.Coaches, src => src.MapFrom(p => p.CoachPlayers.Select(cp => cp.Coach.FirstName + " " + cp.Coach.LastName)))
                .ForMember(dst => dst.Name, src => src.MapFrom(cp => cp.FirstName + " " + cp.LastName));
        }
    }
}
