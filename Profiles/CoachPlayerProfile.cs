﻿using AutoMapper;
using Football_Academy_API.DTOs.CoachPlayer;
using Football_Academy_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Football_Academy_API.Profiles
{
    public class CoachPlayerProfile : Profile
    {
        public CoachPlayerProfile()
        {
            CreateMap<CoachPlayer, GetCoachPlayersDTO>();
        }
    }
}
