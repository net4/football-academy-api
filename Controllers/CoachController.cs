﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Football_Academy_API.DTOs.Coach;
using Football_Academy_API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Football_Academy_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoachController : ControllerBase
    {
        private readonly AcademyContext _context;
        private readonly IMapper _mapper;

        public CoachController(AcademyContext context, IMapper imapper)
        {
            _context = context;
            _mapper = imapper;
        }

        /// <summary>
        /// Retrive Coach with given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<GetCoachDTO> GetCoach(int id)
        {
            var coach = _context.Coaches.Include(coach => coach.CoachPlayers).ThenInclude(cp => cp.Player).FirstOrDefault(coach => coach.Id == id);
            var coachDto = _mapper.Map<GetCoachDTO>(coach);
            return coachDto;

            //var player = _context.Players.Include(player => player.CoachPlayers).ThenInclude(cp => cp.Coach).FirstOrDefault(player => player.Id == id);
            //var playerDto = _mapper.Map<GetPlayerDTO>(player);
            //return playerDto;
        }

        /// <summary>
        /// Method to retrive all coaches
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<Coach>> GetAllCoaches()
        {
            var allCoaches = _context.Coaches.Select(coach => coach).Include(coach => coach.CoachPlayers).ToList();
            return allCoaches;
        }

        /// <summary>
        /// Delete Coach with given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public ActionResult<Coach> DeleteCoach(int id)
        {

            var coach = _context.Coaches.Find(id);
            if (coach == null)
            {
                return NotFound();
            }
            else
            {
                _context.Coaches.Remove(coach);
                _context.SaveChanges();
                return coach;
            }
        }

        /// <summary>
        /// Create new Coach object to be stored in the database
        /// </summary>
        /// <param name="coach">Player Object</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<Coach> CreatePlayer(Coach coach)
        {
            _context.Coaches.Add(coach);
            _context.SaveChanges();

            return coach;
        }

        /// <summary>
        /// Method to update Coach
        /// </summary>
        /// <param name="id">Provde players primary id key</param>
        /// <param name="coach">Player properties</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public ActionResult UpdateCoach(int id, Coach coach)
        {
            if (id != coach.Id)
            {
                return BadRequest();
            }
            else
            {
                _context.Entry(coach).State = EntityState.Modified;
                _context.SaveChanges();

                return NoContent();
            }

        }
    }
}
