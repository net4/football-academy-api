﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Football_Academy_API.DTOs.CoachPlayer;
using Football_Academy_API.DTOs.Player;
using Football_Academy_API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Football_Academy_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoachPlayerController : ControllerBase
    {
        private readonly AcademyContext _context;
        public readonly IMapper _mapper;
        public CoachPlayerController(AcademyContext context, IMapper imapper)
        {
            _context = context;
            _mapper = imapper;
        }

        /// <summary>
        /// Method to retrive all coach player relations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<GetCoachPlayersDTO>> GetAllCoachPlayers()
        {
            var allCoachPlayers = _context.CoachPlayers.Select(coachplayer => coachplayer).ToList();

            List<GetCoachPlayersDTO> allCoachPlayersDTO = new List<GetCoachPlayersDTO>();
            foreach (var coachplayer in allCoachPlayers)
            {
                var playerDto = _mapper.Map<GetCoachPlayersDTO>(coachplayer);
                allCoachPlayersDTO.Add(playerDto);

            }
            return allCoachPlayersDTO;
        }

        /// <summary>
        /// Create new CoachPlayer object to be stored in the database
        /// </summary>
        /// <param name="coach">Player Object</param>
        /// <returns></returns>
        [HttpPost()]
        public ActionResult<CoachPlayer> CreatePlayer(CoachPlayer coachplayer)
        {
            _context.CoachPlayers.Add(coachplayer);
            _context.SaveChanges();

            return coachplayer;
        }

        /// <summary>
        /// Delete relation between player and coach
        /// </summary>
        /// <param name="playerid"></param>
        /// <param name="coachid"></param>
        /// <returns></returns>
        [HttpDelete("{playerid}-{coachid}")]
        public ActionResult<CoachPlayer> DeleteCoachPlayer (int playerid, int coachid)
        {
            var coachplayer = _context.CoachPlayers.Single(cp => cp.CoachId == coachid && cp.PlayerId == playerid);
            _context.CoachPlayers.Remove(coachplayer);
            _context.SaveChanges();

            return coachplayer;
        }

    }
}
