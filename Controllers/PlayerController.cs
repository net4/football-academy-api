﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Football_Academy_API.DTOs.Player;
using Football_Academy_API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Football_Academy_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        private readonly AcademyContext _context;
        private readonly IMapper _mapper;
        public PlayerController (AcademyContext context, IMapper imapper)
        {
            _context = context;
            _mapper = imapper;
        }


        /// <summary>
        /// Retrive player with given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<GetPlayerDTO> GetPlayer(int id)
        {
            var player = _context.Players.Include(player => player.CoachPlayers).ThenInclude(cp => cp.Coach).FirstOrDefault(player => player.Id == id);
            var playerDto = _mapper.Map<GetPlayerDTO>(player);
            return playerDto;
        }

        /// <summary>
        /// Async method to retrive all players
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult <IEnumerable<Player>>> GetAllPlayers()
        {
            return await _context.Players.Select(player => player).Include(player => player.CoachPlayers).ToListAsync();
        }

        /// <summary>
        /// Delete player with given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public ActionResult<Player> DeletePlayer(int id){

            var player = _context.Players.Find(id);
            if (player == null)
            {
                return NotFound();
            }
            else
            {
                _context.Players.Remove(player);
                _context.SaveChanges();
                return player;
            }
        }

        /// <summary>
        /// Create new player object to be stored in the database
        /// </summary>
        /// <param name="player">Player Object</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<Player> CreatePlayer(Player player)
        {
            _context.Players.Add(player);
            _context.SaveChanges();

            //return CreatedAtAction("CreatedNewPlayer", new Player(), player);
            return player;
        }

        /// <summary>
        /// Method to update Player
        /// </summary>
        /// <param name="id">Provde players primary id key</param>
        /// <param name="player">Player properties</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public ActionResult UpdatePlayer(int id, Player player)
        {
            if (id != player.Id)
            {
                return BadRequest();
            }else{
                _context.Entry(player).State = EntityState.Modified;
                _context.SaveChanges();

                return NoContent();
            }

        }
    }
}
