﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Football_Academy_API.Models
{
    public class Player
    {
        //Setting properties for Players at the Academy
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Nationality { get; set; }
        public string Position { get; set; }
        public ICollection<CoachPlayer> CoachPlayers { get; set; }


    }
}
