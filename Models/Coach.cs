﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Football_Academy_API.Models
{
    public class Coach
    {
        //Properties for Coaches
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Nationality { get; set; }
        public string Expertise { get; set; }
        public ICollection<CoachPlayer> CoachPlayers { get; set; }
    }
}
