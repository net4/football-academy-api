﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Football_Academy_API.Models
{
    public class CoachPlayer
    {
        //Setting Properties for a many to many relationship between Players and Coaches
        public int PlayerId { get; set; }
        public Player Player { get; set; }
        public int CoachId { get; set; }
        public Coach Coach { get; set; }
    }
}
