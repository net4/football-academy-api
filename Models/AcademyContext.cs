﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Football_Academy_API.Models
{
    public class AcademyContext : DbContext
    {
        public DbSet<Player> Players { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<CoachPlayer> CoachPlayers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CoachPlayer>().HasKey(sa => new { sa.PlayerId, sa.CoachId });
        }

        public AcademyContext(DbContextOptions<AcademyContext>options) :base(options)
        {

        }
    }
}
